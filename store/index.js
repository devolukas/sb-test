import Vue from 'vue'

const types = {
  /* SET_APPLICATION_LOCALE: 'SET_APPLICATION_LOCALE', */
  SET_CMS_CACHE_VERSION: 'SET_CMS_CACHE_VERSION',
  SET_APPLICATION_SETTINGS: 'SET_APPLICATION_SETTINGS',
  SET_APPLICATION_ERROR_PAGE: 'SET_APPLICATION_ERROR_PAGE',
}

export const state = () => ({
  settings: {
    header: [],
    footer: [],
    mobileNavigation: [],
    cookieConsent: [],
    /*     locales: [],
    fallbackLocale: 'en', */
    brandSlug: '',
    brandSlugDelimiter: '',
  },
  errorPageData: {
    error404: {},
    error500: {},
    errornetwork: {},
  },
  cmsCacheVersion: +Date.now(),
  /*   locale: 'en', */
})

export const actions = {
  /*   setApplicationLocale({ commit }, locale) {
    // check if you are on a different language page - if so go to correct one
    commit(types.SET_APPLICATION_LOCALE, locale)
  }, */
  refreshCMSCacheVersion({ commit }) {
    commit(types.SET_CMS_CACHE_VERSION)
  },
  async fetchApplicationSettings(
    { commit, dispatch, getters, state },
    version
  ) {
    if (new Date(getters.cmsLastCacheFetch + 10800000) < new Date()) {
      // if there is no cache version set yet or 3 hours (10.800.000) passed since the last fetch of the cache version -> fetch a new version
      await dispatch('refreshCMSCacheVersion')
    }

    // fetch application settings (header, footer, ...) + error pages
    try {
      const cmsSettingsResult = await this.$storyapi.get('cdn/stories', {
        version,
        starts_with: `en/settings`, // getters.locale
      })
      cmsSettingsResult.data.stories.forEach((story) => {
        if (story.slug === 'settings') {
          commit(types.SET_APPLICATION_SETTINGS, story.content)
        }

        if (story.slug.includes('error')) {
          commit(types.SET_APPLICATION_ERROR_PAGE, story)
        }
      })
    } catch (err) {
      console.log(err)
    }
  },
}

export const mutations = {
  /*   [types.SET_APPLICATION_LOCALE](state, locale) {
    if (state.locales.includes(locale)) {
      Vue.set(state, 'locale', locale)
      this.app.i18n.locale = locale
    }
  }, */
  [types.SET_CMS_CACHE_VERSION](state) {
    Vue.set(state, 'cmsCacheVersion', +Date.now())
  },
  [types.SET_APPLICATION_SETTINGS](state, settings) {
    Vue.set(state, 'settings', settings)
  },
  [types.SET_APPLICATION_ERROR_PAGE](state, errorPage) {
    Vue.set(state.errorPageData, errorPage.slug, errorPage.content)
  },
}

export const getters = {
  /*   locales: (state) => {
    return state.settings.locales
  },
  fallbackLocale: (state) => {
    return state.settings.fallbackLocale
  }, */
  brandSlug: (state) => {
    return state.settings.brandSlug
  },
  brandSlugDelimiter: (state) => {
    return state.settings.brandSlugDelimiter
  },
  header: (state) => {
    return state.settings.header
  },
  footer: (state) => {
    return state.settings.footer
  },
  mobileNavigation: (state) => {
    return state.settings.mobileNavigation
  },
  errorPageData: (state) => {
    return state.errorPageData
  },
  cookieConsent: (state) => {
    return state.settings.cookieConsent
  },
  cmsCacheVersion: (state) => {
    return state.cmsCacheVersion
  },
  /*   locale: (state) => {
    return state.locale
  }, */
}
