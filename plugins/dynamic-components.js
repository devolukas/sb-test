import Vue from 'vue'
/* Common Components */
import Page from '~/components/dynamic/common/Page.vue'
import Link from '~/components/dynamic/common/Link.vue'
Vue.component('st-page', Page)
Vue.component('st-link', Link)

/*
import Grid from '~/components/core/Grid.vue'
Vue.component('grid', Grid)
import Column from '~/components/core/Column.vue'
Vue.component('column', Column)
import StructuredData from '~/components/core/StructuredData.vue'
Vue.component('structuredData', StructuredData)
import Spacer from '~/components/core/Spacer.vue'
Vue.component('spacer', Spacer) */

/* Project Specific Components */

/*
import Skeleton from '~/components/core/Skeleton.vue'
import CallToAction from '~/components/core/CallToAction.vue'
import NavItem from '~/components/core/NavItem.vue'
import ImageNavItem from '~/components/core/ImageNavItem.vue'

import Icon from '~/components/core/Icon.vue'
import Tag from '~/components/core/Tag.vue'
import Heading from '~/components/core/Heading.vue'
import Breadcrumbs from '~/components/core/Breadcrumbs.vue'
import List from '~/components/core/List.vue'
import ListItem from '~/components/core/ListItem.vue'
import BackgroundImage from '~/components/core/BackgroundImage.vue'
import BackgroundVideo from '~/components/core/BackgroundVideo.vue'
import Avatar from '~/components/core/Avatar.vue'
import Overlay from '~/components/core/Overlay.vue'
import Hero from '~/components/core/Hero.vue'
import Teaser from '~/components/core/Teaser.vue'
import Box from '~/components/core/Box'
import Card from '~/components/core/Card.vue'
import Quote from '~/components/core/Quote'
import QAItem from '~/components/core/QAItem'
import Product from '~/components/core/Product'
import Number from '~/components/core/Number'
import ContentSection from '~/components/core/ContentSection.vue'
import SectionText from '~/components/core/SectionText'
import ContactForm from '~/components/core/ContactForm.vue' */

/* specific Components */
/* import CustomSvg from '~/components/specific/CustomSvg.vue'
import CustomPath from '~/components/specific/CustomPath.vue' */

/* Vue.component('skeleton', Skeleton)
Vue.component('callToAction', CallToAction)
Vue.component('navItem', NavItem)
Vue.component('imageNavItem', ImageNavItem)
Vue.component('logo', Logo) */

/* Vue.component('Icon', Icon)
Vue.component('tag', Tag)
Vue.component('heading', Heading)
Vue.component('breadcrumbs', Breadcrumbs)
Vue.component('list', List)
Vue.component('listItem', ListItem)
Vue.component('backgroundImage', BackgroundImage)
Vue.component('backgroundVideo', BackgroundVideo)
Vue.component('avatar', Avatar)
Vue.component('overlay', Overlay)
Vue.component('hero', Hero)
Vue.component('teaser', Teaser)
Vue.component('box', Box)
Vue.component('aCard', Card)
Vue.component('quote', Quote)
Vue.component('qAItem', QAItem)
Vue.component('product', Product)
Vue.component('number', Number)
Vue.component('contentSection', ContentSection)
Vue.component('sectionText', SectionText)
Vue.component('contactForm', ContactForm)


Vue.component('customSvg', CustomSvg)
Vue.component('customPath', CustomPath) */
