import pkg from './package'

export default {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    bodyAttrs: {
      class: [
        'font-sans font-medium bg-light-surface dark:bg-dark-surface text-light-onSurface dark:text-dark-onSurface transition-colors duration-300 ease-linear ',
      ],
    },
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' }, // false

  /*
   ** Global CSS
   */
  css: [],
  /*
   ** router middleware run at every page
   */
  router: {
    // checks for falsy urls, corrects them and sets appropriate languages too
    middleware: 'default-guard',
  },

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [{ src: '~/plugins/dynamic-components' }],

  /*
   ** Build Modules
   */
  buildModules: [
    [
      '@nuxtjs/tailwindcss',
      {
        purgeCSSInDev: false,
        exposeConfig: true,
      },
    ],
    [
      '@nuxtjs/color-mode',
      {
        // preference: 'light' // disable system
      },
    ],
  ],
  purgeCSS: {
    /* whitelist: ['css-selector-to-whitelist'], */
  },

  /*
   ** Nuxt.js modules
   */
  modules: [
    [
      'storyblok-nuxt',
      {
        accessToken: process.env.SB_PREV_TOKEN || 'no-token',
        cacheProvider: 'memory',
      },
    ],
  ],

  /*
   ** Nuxt.js Environment Variables accesible on Client Side
   */
  env: {
    // base Url needed for some redirections and info
    baseUrl: process.env.BASE_URL || 'http://no-base-url-specified',
  },

  /*
   ** Static Generation of pages
   */
  generate: {
    fallback: true, // Fallback to SPA Mode to display 404 pages correctly if surfing to a page that is not generated
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
}
