/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
const plugin = require('tailwindcss/plugin')
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  theme: {
    screens: {
      xs: '320px',
      ...defaultTheme.screens,
    },
    extend: {
      fontFamily: {
        sans: [
          'Quicksand',
          // ...defaultTheme.fontFamily.sans
          "-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, 'Helvetica Neue', Arial, 'Noto Sans', sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol', 'Noto Color Emoji'",
        ],
      },
      fontSize: {
        ss: '0.666666rem',
      },
      colors: {
        link: {
          default: '#ea1741',
        },
        primary: {
          default: '#ea1741',
          light: '#eb274e',
          dark: '#dc143c',
        },
        light: {
          surface: '#d9d9d9',
          elevatedSurface: '#eaeaea',
          onSurface: '#2F495E',
          onElevatedSurface: '#606F7B',
          border: '#c8c8c8', // defaultTheme.colors.gray['400'],
        },
        dark: {
          surface: '#181818',
          elevatedSurface: '#202020',
          onSurface: '#dadada',
          onElevatedSurface: '#606F7B',
          border: '#393939',
        },
      },
      fill: (theme) => ({
        primary: theme('colors.primary'),
        'brand-gray': theme('colors.brand.gray'),
        'brand-lightgreen': theme('colors.brand.lightgreen'),
        'brand-green': theme('colors.brand.green'),
      }),
      stroke: (theme) => ({
        'brand-gray': theme('colors.brand.gray'),
        'brand-lightgreen': theme('colors.brand.lightgreen'),
        'brand-green': theme('colors.brand.green'),
      }),
      /*  boxShadow: {
        brand: '0px 0px 8px rgba(0, 0, 0, 0.101562)',
      },
      inset: {
        24: '6rem',
      },
      maxWidth: {
        '1/4': '25%',
        '1/2': '50%',
        '3/4': '75%',
      },
      transitionTimingFunction: {
        'ease-in-material': 'cubic-bezier(0.4, 0, 1, 1)',
        'ease-out-material': 'cubic-bezier(0, 0, 0.2, 1)',
        'ease-in-out-material': 'cubic-bezier(0.4, 0, 0.2, 1)',
        'ease-in-out-material-sharp': 'cubic-bezier(0.4, 0, 0.6, 1)',
      }, */
    },
  },
  variants: {
    display: ['responsive', 'after'],
    margin: ['responsive', 'after'],
    width: ['responsive', 'after'],
    borderWidth: ['responsive', 'after'],
    borderRadius: ['responsive', 'after'],
    borderColor: [
      'responsive',
      'hover',
      'focus',
      'dark',
      'light',
      'after',
      'light:after',
      'dark:after',
    ],
    backgroundColor: [
      'responsive',
      'hover',
      'focus',
      'dark',
      'light',
      'dark:hover',
      'light:hover',
    ],
    textColor: [
      'responsive',
      'hover',
      'focus',
      'group-hover',
      'dark',
      'light',
      'dark:hover',
      'light:hover',
    ],
  },
  corePlugins: {
    container: false,
  },
  plugins: [
    plugin(function ({ addVariant, theme, e, prefix, config }) {
      const colorModeVariants = ['light', 'dark']
      colorModeVariants.forEach((mode) => {
        addVariant(mode, ({ modifySelectors, separator }) => {
          modifySelectors(({ className }) => {
            return `.${mode}-mode .${e(`${mode}${separator}${className}`)}`
          })
        })
      })
      const pseudoVariants = ['after', 'before']
      pseudoVariants.forEach((pseudo) => {
        addVariant(pseudo, ({ modifySelectors, separator }) => {
          modifySelectors(({ className }) => {
            return `.${e(`${pseudo}${separator}${className}`)}::${pseudo}`
          })
        })
      })
      // generate chained color mode and pseudo variants
      colorModeVariants.forEach((mode) => {
        pseudoVariants.forEach((pseudo) => {
          addVariant(`${mode}:${pseudo}`, ({ modifySelectors, separator }) => {
            modifySelectors(({ className }) => {
              return `.${mode}-mode .${e(
                `${mode}${separator}${pseudo}${separator}${className}`
              )}::${pseudo}`
            })
          })
        })
      })
      // states for color modes
      const states = ['hover']
      colorModeVariants.forEach((mode) => {
        states.forEach((state) => {
          addVariant(`${mode}:${state}`, ({ modifySelectors, separator }) => {
            modifySelectors(({ className }) => {
              return `.${mode}-mode .${e(
                `${mode}${separator}${state}${separator}${className}`
              )}:${state}`
            })
          })
        })
      })
    }),
  ],
}
