export default async function (to, from, savedPosition) {
  if (savedPosition) {
    return savedPosition
  }
  // eslint-disable-next-line
  const findEl = async (hash, x) => {
    return (
      document.querySelector(hash) ||
      new Promise((resolve, reject) => {
        if (x > 50) {
          return resolve()
        }
        setTimeout(() => {
          resolve(findEl(hash, ++x || 1))
        }, 100)
      })
    )
  }

  if (to.hash) {
    const el = await findEl(to.hash)
    // get header height
    const header = document.querySelector('#header-navigation')
    if ('scrollBehavior' in document.documentElement.style) {
      return window.scrollTo({
        top: el.offsetTop - header.clientHeight,
        behavior: 'smooth',
      })
    } else {
      return window.scrollTo(0, el.offsetTop - header.clientHeight)
    }
  }

  return { x: 0, y: 0 }
}
