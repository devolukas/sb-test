/*
 * check url for possible redirects needed
 */
export default function ({ app, store, route, params, redirect, isDev }) {
  // load application settings from cms
  if (!store.state.settings._uid) {
    /*    // check if language has changed and correct in store and i18n module if so
   if (store.state.locale !== params.lang) {
     store.dispatch('setApplicationLocale', params.lang)
   }
   if (app.i18n.locale !== params.lang) {
     app.i18n.locale = store.state.locale
   } */
    // get the application settings from the CMS
    return store.dispatch(
      'fetchApplicationSettings',
      route.query._storyblok || isDev ? 'draft' : 'published'
    )
  } else if (
    store.state.cmsCacheVersion === '' ||
    new Date(store.getters.cmsLastCacheFetch + 10800000) < new Date()
  ) {
    // if there is no cache version set yet or 3 hours (10.800.000) passed since the last fetch of the cache version -> fetch a new version
    return store.dispatch('refreshCMSCacheVersion')
  }

  /*  let redirectUrl = ''
  let redirectType = 302

  if (params.lang) {
    // if the url has a language but its not an available one in the app then redirect to default and add the remaining path
    if (store.state.locales.indexOf(params.lang) === -1) {
      if (route.path.length > 1 + params.lang.length) {
        redirectUrl = `${app.i18n.fallbackLocale}${route.path.substr(
          1 + params.lang.length
        )}`
      } else {
        redirectUrl = `${app.i18n.fallbackLocale}`
      }
    } else {
      // if cache has to be flushed -> fetch a new version
      if (process.server && route.query._cacheflush) {
        // flush cache and update cache version
        app.$storyapi.flushCache()
      }

      // load application settings from cms
      const version = route.query._storyblok || isDev ? 'draft' : 'published'

      if (!store.state.settings._uid || store.state.locale !== params.lang) {
        // check if language has changed and correct in store and i18n module if so
        if (store.state.locale !== params.lang) {
          store.dispatch('setApplicationLocale', params.lang)
        }
        if (app.i18n.locale !== params.lang) {
          app.i18n.locale = store.state.locale
        }
        // get the application settings from the CMS
        return store.dispatch('fetchApplicationSettings', version)
      } else if (
        store.state.cmsCacheVersion === '' ||
        new Date(store.getters.cmsLastCacheFetch + 10800000) < new Date()
      ) {
        // if there is no cache version set yet or 3 hours (10.800.000) passed since the last fetch of the cache version -> fetch a new version
        return store.dispatch('fetchCMSCacheVersion')
      }
    }
  } else {
    // if url does not have language, redirect to default language
    redirectUrl = `${app.i18n.fallbackLocale}`
    redirectType = 301
  }

  // redirect is needed
  if (redirectUrl !== '') {
    return redirect(redirectType, `/${redirectUrl}`)
  } */
}
